import http from "./http"

// 基本配置接口
function getFrSet(data) {
	return http({
		url: '/wp-json/wp/v2/fengrui?&order=asc',
		data,
		method: "GET",
	})
}

// 轮播接口
function getLunbo(data) {
	return http({
		url: '/wp-json/wp/v2/posts?include=' + data,
		data,
		method: "GET",
	})
}

// 获取顶部接口
function getCategIndex(data) {
	return http({
		url: '/wp-json/wp/v2/categories?per_page=100&include=' + data,
		data,
		method: "GET",
	})
}

// 文章列表接口
function getList(page, filler) {
	return http({
		url: '/wp-json/wp/v2/posts?page=' + page + '&categories_exclude=' + filler,
		method: "GET",
	})
}

// 文章详情接口
function getInfo(data, pass) {
	return http({
		url: '/wp-json/wp/v2/posts/' + data,
		method: "GET",
	})
}

// 文章密码详情接口
function getInfoPass(data, pass) {
	return http({
		url: '/wp-json/wp/v2/posts/' + data + '?password=' + pass,
		method: "GET",
	})
}

// 文章评论接口
function getMessageInfo(data, page) {
	return http({
		url: '/wp-json/wp/v2/comments?post=' + data + '&page=' + page,
		method: "GET",
	})
}

// 分类文章列表接口
function getCategoList(active, page) {
	return http({
		url: '/wp-json/wp/v2/posts?categories=' + active + '&page=' + page,
		method: "GET",
	})
}

// 热门文章接口
function getTagsList(tag) {
	return http({
		url: '/wp-json/wp/v2/posts/?tags=' + tag + '&per_page=3',
		method: "GET",
	})
}


// 获取分类接口
function getCategory(filler, page) {
	return http({
		url: '/wp-json/wp/v2/categories?per_page=20&page=' + page + '&exclude=' + filler,
		method: "GET",
	})
}

// 获取父级分类接口
function getCategoryParent(filler, page) {
	return http({
		url: '/wp-json/wp/v2/categories?per_page=100&parent=0&page=' + page + '&exclude=' + filler,
		method: "GET",
	})
}

// 获取二级分类接口
function getCategoryParentID(filler, page, parent) {
	return http({
		url: '/wp-json/wp/v2/categories?per_page=20&page=' + page + '&parent=' + parent + '&exclude=' + filler,
		method: "GET",
	})
}


// 标签文章接口
function getTagAllList(tag, page, filler) {
	return http({
		url: '/wp-json/wp/v2/posts?tags=' + tag + '&page=' + page + '&categories_exclude=' + filler,
		method: "GET",
	})
}

// 搜索接口
function getSearchList(key, page, filler) {
	return http({
		url: '/wp-json/wp/v2/posts?search=' + key + '&page=' + page + '&categories_exclude=' + filler,
		method: "GET",
	})
}

// 标签接口
function getTags(per, page, offset) {
	return http({
		url: '/wp-json/wp/v2/tags?per_page=' + per + '&orderby=id&order=desc&page=' + page + '&offset=' +
			offset,
		method: "GET",
	})
}
// 标签接口
function getTagsInclude(data) {
	return http({
		url: '/wp-json/wp/v2/tags?include=' + data,
		method: "GET",
	})
}

//卡密接口
const postKeyApi = "https://www.frbkw.com/a/card_code_api.php";

// 导出接口
export {
	// 基本配置接口
	getFrSet,
	// 轮播接口
	getLunbo,
	// 获取顶部接口
	getCategIndex,
	// 文章列表接口
	getList,
	// 分类文章列表接口
	getCategoList,
	// 文章详情接口
	getInfo,
	getInfoPass,
	// 热门文章接口
	getTagsList,
	// 文章评论接口
	getMessageInfo,
	// 获取分类接口
	getCategory,
	// 标签文章接口
	getTagAllList,
	// 搜索接口
	getSearchList,
	// 标签接口
	getTags,
	getTagsInclude,
	//父级分类
	getCategoryParent,
	//二级分类
	getCategoryParentID,
	// 卡密接口
	postKeyApi,
}