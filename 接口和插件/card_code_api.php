<?php
// 检查请求方法是否为 POST
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    http_response_code(405);
    echo "非法请求";
    exit;
}

// 数据库连接配置
$servername = "localhost";
$username = "card_codes";
$dbname = "card_codes";
$password = "rS3frjnmKCL7sHJ6";


// 创建数据库连接
$conn = new mysqli($servername, $username, $password, $dbname);

// 检查连接是否成功
if ($conn->connect_error) {
    die("数据库连接失败: " . $conn->connect_error);
}

// 查询未使用的卡密
$sql = "SELECT id, code FROM card_codes WHERE is_used = false LIMIT 1";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // 获取一条未使用的卡密
    $row = $result->fetch_assoc();
    $cardCode = $row['code'];
    $cardId = $row['id'];

    // 标记该卡密为已使用
    $updateSql = "UPDATE card_codes SET is_used = true WHERE id = $cardId";
    if ($conn->query($updateSql) === TRUE) {
        // 对卡密进行 base64 编码（JavaScript 中的 atob 对应 PHP 的 base64_decode，这里使用 base64_encode）
        $encodedCardCode = base64_encode($cardCode);
        echo $encodedCardCode;
    } else {
        echo "标记卡密为已使用时出错: " . $conn->error;
    }
} else {
    echo "没有可用的卡密";
}

// 关闭数据库连接
$conn->close();
?>    